Read2ReferenceMapper contributors
===============================

* **[Benjamin 'Ben' Bolduc]()**
  
  * Author and maintainer
  * Most features development

* **[Simon Roux]()**

  * Author of 'filter_bam_file_coverage'