# Docker-Read2RefMapper

## Overview

**Read2RefMapper** is a python-wrapper for a number of scripts and tools that allow for filtering coverage of BAM files against a reference dataset. It filters reads matching reference sequences for those references that are not covered over a specified threshold length, as well as alignment identity and alignment coverage. It is designed to be used in conjunction with [Docker-BatchBowtie]().

## Installation

Since everything runs within a Docker container, there is no need to install any dependencies, other than [Docker](https://www.docker.com/).

### Using the Dockerfile

Build the Docker image from the Dockerfile:

```
docker build -t bbolduc/read2ref:dev .
```

The "." in the code above means to use the Dockerfile from the current directory. (the file is literally named "Dockerfile")

Now you can "run" the Docker image - almost as if it's another program on your system.

```
docker run --rm -v $PWD/inputDir:inputDir -w /inputDir bbolduc/read2ref:dev --dir /inputDir
```

Commands after the "bbolduc/read2ref:dev" are being passed to the Dockerized python script.

### Minimal command line options

**--rm**: Remove the Docker container after running.

**-v**: Bind directory from the *host* system to the container system. This is the **absolute host path**, followed by **:**, then where (on the *container's* system) to mount it.

**-w**: Sets working directory *within the container*.

**--dir**: Tells the script *where* to look for BAM input files. By default, it will take all BAM files, ignoring all other files that might be in that directory. For ease of use, this **must be** the same directory that is mounted undre the container.

### Other Options

**--log**: Log file name.

**--num-threads**: Number of threads to use during processing.

**--percent-id**: Minimum allowable percentage base identity of a mapped read (0.0 - 1.0).

**--percent-aln**: Minimum allowable percentage read bases mapped (0.0 - 1.0).

**--coverage-mode**: How to calculate coverage (requires --coverages to be enabled).

**--cov_filter**: Percent of reference that must be coverged to be considered. (0 - 100).

**--metagenome-sizes**: A csv-formatted file containing metagenome name and metagenome size. If provided (along with --coverages), will normalize coverages to the size of their metagenomes.

### Additional Notes

#### BamM Options...

This script wraps [BamM](https://github.com/ecogenomics/BamM), but only passes a small portion of its options/arguments. Future updates will add to the options available.

#### Metagenome Sizes File

The metagenome sizes file (for example, metagenome_bp.csv) should be formatted as a comma-separated file that includes 2 headers, "Site" and "Read BP." During processing (if enabled), the script will read the site/sample names and compare them against the BAM file names. So if a site is named GG204, the 1st read file with "GG204" in its name will be used.

### Authors

* Ben Bolduc

* Simon Roux

For a full list of contributions, see the Contributors file.
